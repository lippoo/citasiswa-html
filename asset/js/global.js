// function openNav() {
//   document.getElementById("main-sidenav").style.width = "250px";
//   document.getElementById("inner-content").style.marginLeft = "250px";
//   document.body.style.backgroundColor = "rgba(0,0,0,0.4)";
// }
//
// function closeNav() {
//   document.getElementById("main-sidenav").style.width = "0";
//   document.getElementById("inner-content").style.marginLeft= "0";
//   document.body.style.backgroundColor = "white";
// }

jQuery(function(){
  jQuery('#dashmenu ul.top-nav li').each(function() {
    if(jQuery('.sub-menu',this).length) {
      jQuery('> a',this).addClass('toggle-submenu');
      jQuery('.sub-menu',this).each(function() {
        jQuery(this).addClass('level-'+jQuery(this).parents('#dashmenu .sub-menu').length);
      });
    }
  });
  jQuery('#dashmenu ul.dash-menu li.has-submenu',this).append('<i class="icon-down-open-big"></i>');

  //subnav toggle
  jQuery('ul.dash-menu i').on('click', function(e){
    e.preventDefault();
    jQuery('#dashmenu li').not(jQuery(this).parents('li')).removeClass('shrink');
    jQuery(this).closest('li').toggleClass('shrink');
  });
});

var app = {
  init: function() {
    app.faqPanel();
    app.newsletterPop();
    app.studentFiles();
    app.donorDashboardThumb();
    app.donorPayDonate();
    app.studentStatusDonate();
    app.mobileMenu();
  },
  faqPanel: function(){
		jQuery('.panel-collapse').on('show.bs.collapse', function () {
			jQuery(this).siblings('.panel-heading').addClass('active');
		});

		jQuery('.panel-collapse').on('hide.bs.collapse', function () {
			jQuery(this).siblings('.panel-heading').removeClass('active');
		});
  },
  newsletterPop: function(){
    if(jQuery('.form-newsletter').length){
      jQuery('.form-newsletter').magnificPopup({
    		type: 'inline',
    		preloader: false,
    		focus: '#name',

    		// When elemened is focused, some mobile browsers in some cases zoom in
    		// It looks not nice, so we disable it:
    		callbacks: {
    			beforeOpen: function() {
    				if($(window).width() < 700) {
    					this.st.focus = false;
    				} else {
    					this.st.focus = '#name';
    				}
    			}
    		}
    	});
    }
  },
  studentFiles: function(){
    // We can attach the `fileselect` event to all file inputs on the page
    jQuery(document).on('change', ':file', function() {
      var input = jQuery(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    jQuery(document).ready( function() {
      jQuery(':file').on('fileselect', function(event, numFiles, label) {

        var input = jQuery(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }

      });
    });
  },
  donorDashboardThumb: function(){
    jQuery(document).on('click', '.panel-heading span.clickable', function(e){
      var $this = jQuery(this);
      if(!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
      } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
      }
    })
  },
  donorPayDonate: function(){
    jQuery('#select-all').click(function(event) {
      if(this.checked) {
        // Iterate each checkbox
        jQuery('.outer-paydonate :checkbox').each(function() {
            this.checked = true;
        });
      } else {
        jQuery('.outer-paydonate :checkbox').each(function() {
              this.checked = false;
          });
      }
    });
  },
  studentStatusDonate: function(){
    jQuery('.chart').easyPieChart({
      easing: "easeOutElastic",
      delay: 3e3,
      barColor: "#5FCAF4",
      trackColor: "#f8f8f8",
      scaleColor: !1,
      lineWidth: 20,
      trackWidth: 16,
      lineCap: "square",
      size: 300
    });
  },
  mobileMenu: function(){
    jQuery('#menu-toggle').click(function(e){
      e.preventDefault();
      jQuery('#content .wrapper').toggleClass("act");
    });
  }
};
jQuery(document).ready(function($){
app.init();
})
